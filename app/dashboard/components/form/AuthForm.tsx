"use client";

import { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";

interface FormValues {
  username: string;
  email: string;
  password: string;
  phone: string;
}

enum FormType {
  Register = "REGISTER",
  Login = "LOGIN",
}

const AuthForm = () => {
  const [currentForm, setCurrentForm] = useState<FormType>(FormType.Register);
  const onSubmit: SubmitHandler<FormValues> = (data) => {
    // Handle form submission logic here
    if (currentForm === FormType.Register) {
      console.log(data, "from register");
    } else {
      console.log(data, "from login");
    }
  };

  const switchForm = () => {
    reset();
    setCurrentForm((prevForm) =>
      prevForm === FormType.Register ? FormType.Login : FormType.Register
    );
  };

  const {
    register,
    handleSubmit,
    formState: { errors },
    reset,
  } = useForm<FormValues>();

  return (
    <div className="bg-gray-900 flex justify-center w-full h-screen p-1">
      <div className="bg-yellow-500 w-full">left side add gif</div>
      <div className="bg-[#1E1E1E] w-full flex items-center justify-center h-full flex-col">
        {currentForm === FormType.Register && (
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="flex flex-col gap-8 w-[45%] h-full justify-center item gap py-16"
          >
            <h2 className="py-.5 w-full text-center border-b border-gray-500 leading-[1px] mx-0 mt-[10px] mb-[20px]">
              <span className="bg-[#1E1E1E] py-0 px-[10px] text-gray-50 font-semibold">
                Register
              </span>
            </h2>

            <div className="relative felx flex-col justify-center items-center">
              <input
                {...register("username")}
                className="py-2 peer bg-[#1E1E1E] h-10 w-full border-2 border-gray-500 text-gray-500 placeholder-transparent focus:outline-none focus:border-[#eb8307]"
                placeholder="john@doe.com"
                autoComplete="off"
                id="username"
                name="username"
                type="text"
              ></input>
              <label
                className="absolute left-1 -top-2.5 text-gray-50 bg-[#1E1E1E] text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-500 peer-placeholder-shown:top-2 peer-focus:-top-2.5 peer-focus:text-gray-50 peer-focus:text-sm peer-focus:bg-[#1E1E1E]"
                htmlFor="username"
              >
                Username
              </label>
            </div>

            <div className="relative felx flex-col justify-center items-center">
              <input
                {...register("email")}
                className="py-2  peer bg-[#1E1E1E] h-10 w-full border-2 border-gray-500 text-gray-500 placeholder-transparent focus:outline-none focus:border-[#eb8307]"
                placeholder="john@doe.com"
                autoComplete="off"
                id="email"
                name="email"
                type="email"
              ></input>
              <label
                className="absolute left-1 -top-2.5 text-gray-50 bg-[#1E1E1E] text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-500 peer-placeholder-shown:top-2 peer-focus:-top-2.5 peer-focus:text-gray-50 peer-focus:text-sm peer-focus:bg-[#1E1E1E]"
                htmlFor="email"
              >
                Email
              </label>
            </div>

            <div className="relative felx flex-col justify-center items-center">
              <input
                {...register("password")}
                className="py-2  peer bg-[#1E1E1E] h-10 w-full border-2 border-gray-500 text-gray-500 placeholder-transparent focus:outline-none focus:border-[#eb8307]"
                placeholder="john@doe.com"
                autoComplete="off"
                id="password"
                name="password"
                type="password"
              ></input>
              <label
                className="absolute left-1 -top-2.5 text-gray-50 bg-[#1E1E1E] text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-500 peer-placeholder-shown:top-2 peer-focus:-top-2.5 peer-focus:text-gray-50 peer-focus:text-sm peer-focus:bg-[#1E1E1E]"
                htmlFor="password"
              >
                Password
              </label>
            </div>

            <div className="relative felx flex-col justify-center items-center">
              <input
                {...register("phone")}
                className="py-2 peer bg-[#1E1E1E] h-10 w-full border-2 border-gray-500 text-gray-500 placeholder-transparent focus:outline-none focus:border-[#eb8307]"
                placeholder="john@doe.com"
                autoComplete="off"
                id="phone"
                name="phone"
                type="text"
              ></input>
              <label
                className="absolute left-1 -top-2.5 text-gray-50 bg-[#1E1E1E] text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-500 peer-placeholder-shown:top-2 peer-focus:-top-2.5 peer-focus:text-gray-50 peer-focus:text-sm peer-focus:bg-[#1E1E1E]"
                htmlFor="phone"
              >
                Phone number
              </label>
            </div>

            <input
              type="submit"
              value={"Sign Up"}
              className="bg-[#eb8307] cursor-pointer hover:bg-[#C67006] text-gray-100 px-4 py-2 rounded-md"
            ></input>
          </form>
        )}

        {currentForm === FormType.Login && (
          <form
            onSubmit={handleSubmit(onSubmit)}
            className="flex flex-col gap-8 w-[45%] h-full justify-center"
          >
            <h2 className="py-.5 w-full text-center border-b border-gray-500 leading-[1px] mx-0 mt-[10px] mb-[20px]">
              <span className="bg-[#1E1E1E] py-0 px-[10px] text-gray-50 font-semibold">
                Login
              </span>
            </h2>

            <div className="relative felx flex-col justify-center items-center">
              <input
                {...register("email")}
                className="py-2 peer bg-[#1E1E1E] h-10 w-full border-2 border-gray-500 text-gray-500 placeholder-transparent focus:outline-none focus:border-[#eb8307]"
                placeholder="john@doe.com"
                autoComplete="off"
                id="email"
                name="email"
                type="email"
              ></input>
              <label
                className="absolute left-1 -top-2.5 text-gray-50 bg-[#1E1E1E] text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-500 peer-placeholder-shown:top-2 peer-focus:-top-2.5 peer-focus:text-gray-50 peer-focus:text-sm peer-focus:bg-[#1E1E1E]"
                htmlFor="email"
              >
                Email
              </label>
            </div>

            <div className="relative felx flex-col justify-center items-center">
              <input
                {...register("password")}
                className="py-2  peer bg-[#1E1E1E] h-10 w-full border-2 border-gray-500 text-gray-500 placeholder-transparent focus:outline-none focus:border-[#eb8307]"
                placeholder="john@doe.com"
                autoComplete="off"
                id="password"
                name="password"
                type="password"
              ></input>
              <label
                className="absolute left-1 -top-2.5 text-gray-50 bg-[#1E1E1E] text-sm transition-all peer-placeholder-shown:text-base peer-placeholder-shown:text-gray-500 peer-placeholder-shown:top-2 peer-focus:-top-2.5 peer-focus:text-gray-50 peer-focus:text-sm peer-focus:bg-[#1E1E1E]"
                htmlFor="password"
              >
                Password
              </label>
            </div>

            <input
              type="submit"
              value={currentForm === FormType.Login ? "Sign In" : "Sign Up"}
              className="bg-[#eb8307] cursor-pointer hover:bg-[#C67006] text-gray-100 px-4 py-2 rounded-md"
            ></input>
          </form>
        )}
        <h2 className="w-[45%] text-center border-b border-gray-500 leading-[1px] mx-0 mt-[10px] mb-[20px]">
          <span className="bg-[#1E1E1E] py-0 px-[10px] text-gray-100">Or</span>
        </h2>

        <div className="flex justify-center items-start h-full w-[45%]">
          <button
            onClick={switchForm}
            type="button"
            className="w-full bg-[#292929] cursor-pointer hover:bg-[#C67006] text-gray-100 px-4 py-2 rounded-md"
          >
            {currentForm === FormType.Login
              ? "Don't have an account?"
              : "Already have an account?"}
          </button>
        </div>
      </div>
    </div>
  );
};

export default AuthForm;

// This is roughly how I'd do it: the line is created by setting a border-bottom on the containing h2 then giving the h2 a smaller line-height. The text is then put in a nested span with a non-transparent background.

// h2 {
//    width: 100%;
//    text-align: center;
//    border-bottom: 1px solid #000;
//    line-height: 0.1em;
//    margin: 10px 0 20px;
// }

// h2 span {
//     background:#fff;
//     padding:0 10px;
// }

// <h2><span>THIS IS A TEST</span></h2>
// <p>this is some content other</p>
