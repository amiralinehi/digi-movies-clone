"use client";
import Image from "next/image";
import SpinnerLoader from "../SpinnerLoader/SpinnerLoader";
import { useState } from "react";
import { IoIosArrowBack } from "react-icons/io";
import { IoIosArrowForward } from "react-icons/io";
import { MdOutlineFavoriteBorder } from "react-icons/md";
import { FiMinus } from "react-icons/fi";
import { IoMdAdd } from "react-icons/io";

const Carousel = ({ slides }: any) => {
  const [isFav, setIsFev] = useState(true);
  const [current, setCurrent] = useState(0);
  const [isLoading, setISLoading] = useState(false);

  const addToFev = () => {
    setISLoading(true);
    setIsFev(!isFav);
    setTimeout(() => {
      setISLoading(false);
    }, 1000);
  };

  const prevSlide = () => {
    if (current === 0) {
      setCurrent(slides.length - 1);
    } else {
      setCurrent(current - 1);
    }
  };

  const nextSlide = () => {
    if (current === slides.length - 1) {
      setCurrent(0);
    } else {
      setCurrent(current + 1);
    }
  };

  return (
    <div className="flex w-full h-full justify-center items-center relative bg-[#050505]">
      {/* left one */}
      <div className="w-[228px] h-[335px]">
        <Image
          width={228}
          height={335}
          alt="prev-slide"
          src={`${
            current === 0
              ? slides[slides.length - 1].secondImage
              : slides[current - 1].secondImage
          }`}
          className="h-full w-full rounded-md"
        />
      </div>
      <div className="overflow-hidden w-[702px] h-[full]">
        <div
          className={`flex transition ease-out duration-40`}
          style={{
            transform: `translateX(-${current * 100}%)`,
          }}
        >
          {slides.map((slide: any) => {
            return (
              <div className="flex-shrink-0" key={slide.key}>
                <Image
                  width={702}
                  height={335}
                  src={slide.firstImage}
                  key={slide.key}
                  alt="slider-image"
                  className="rounded-md"
                />
              </div>
            );
          })}
        </div>
      </div>

      {/* right one */}
      <div className="w-[228px] h-[335px]">
        <Image
          width={228}
          height={335}
          alt="next-slide"
          src={`${
            current === slides.length - 1
              ? slides[0].secondImage
              : slides[current + 1].secondImage
          }`}
          className="w-full h-full rounded-md"
        />
      </div>

      <div className="absolute top-0 flex justify-between items-center w-full h-full">
        <button
          onClick={nextSlide}
          className="bg-[#1E1E1E] rounded-[100%] ml-44 p-2"
        >
          <IoIosArrowBack size={20} color="lightgray"></IoIosArrowBack>
        </button>

        <button
          onClick={prevSlide}
          className="bg-[#1E1E1E] rounded-[100%] mr-44 p-2"
        >
          <IoIosArrowForward size={20} color="lightgray"></IoIosArrowForward>
        </button>
      </div>

      <div
        dir="rtl"
        className="text-sm bg-[#050505] text-[#EB8307] border-[#EB8307]  border-2 absolute bottom-[100%] left-[14%] -top-8 w-fit h-fit px-2 py-0.5 rounded-full font-vazir"
      >
        فیلم های برگزیده
      </div>

      <div className="text-white absolute bottom-[10%] left-[27%] right-2 w-10 h-10 bg-transparent flex flex-col justify-center items-start">
        <div>
          <span className="text-2xl text-yellow-400">
            {slides[current].score}
          </span>
          /<span className="text-sm">100</span>
        </div>
        <div className="rounded-md text-black font-bold  w-fit h-fit bg-yellow-400 text-center px-1 flex flex-col items-center justify-center">
          <div className="text-center w-full h-full box-border">IMDb</div>
        </div>
        <div className="text-2xl mt-1 text-nowrap w-fit h-fit">
          {slides[current].title.toUpperCase()}
        </div>
      </div>

      <div
        onClick={addToFev}
        className="flex items-center bg-transparent w-8 h-8 rounded-full absolute right-[30%] bottom-[10%] border-2 border-gray-400"
      >
        <div className="cursor-pointer p-0 absolute bg-transparent rounded-full left-[8%]">
          {!isLoading && (
            <MdOutlineFavoriteBorder
              color="white"
              size="24px"
            ></MdOutlineFavoriteBorder>
          )}
          {isLoading && <SpinnerLoader></SpinnerLoader>}
        </div>
        <div className="text-sm bg-white rounded-full absolute -right-[45%] top-[30%]">
          {isFav && <FiMinus size={15} color="gray"></FiMinus>}
          {!isFav && <IoMdAdd size={15} color="gray"></IoMdAdd>}
        </div>
      </div>
    </div>
  );
};

export default Carousel;
