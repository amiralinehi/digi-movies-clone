const ListItem = ({ title }: any) => {
  return (
    <li className="flex justify-center items-center gap-1">
      <div className="bg-[#EB8307]  w-2.5 h-2.5 rounded-[30%] rotate-45"></div>
      <p className="text-white">{title}</p>
    </li>
  );
};

export default ListItem;
