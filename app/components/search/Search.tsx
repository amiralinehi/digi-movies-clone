"use client";

import { useEffect, useState } from "react";
import { FaToggleOff } from "react-icons/fa6";
import { FaToggleOn } from "react-icons/fa6";
import { CiSearch } from "react-icons/ci";
import { TbDatabaseSearch } from "react-icons/tb";

interface searchProps {
  actor: string;
  director: string;
  genre: string;
  quality: string;
  order: string;
  maxYearSlider: number;
  minYearSlider: number;
  isMovie: boolean;
  isSeries: boolean;

  onHandleToggleIsMovie: () => void;
  onHandleToggleSeries: () => void;
  onActorChange: (actor: any) => void;
  onDirectorChange: (director: any) => void;
  onGenreChange: (genre: any) => void;
  onQualityChange: (quality: any) => void;
  onOrderChange: (order: any) => void;
  onMaxYearSliderChange: (MaxyearSlider: any) => void;
  onMinYearSliderChange: (MinyearSlider: any) => void;

  onSearch: () => void;
}

const Search: React.FC<searchProps> = ({
  actor,
  director,
  genre,
  quality,
  order,
  maxYearSlider,
  minYearSlider,
  isMovie,
  isSeries,
  onActorChange,
  onDirectorChange,
  onGenreChange,
  onQualityChange,
  onOrderChange,
  onMaxYearSliderChange,
  onMinYearSliderChange,
  onHandleToggleIsMovie,
  onHandleToggleSeries,

  onSearch,
}) => {
  const [scoreSlider, setScorerSlider] = useState(10);

  // const [isMovie, setIsMovie] = useState(true);
  // const [isSeries, setIsSeries] = useState(true);

  // useEffect(() => {
  //   setIsMovie(false);
  //   setIsSeries(false);
  // }, []);

  const [isDubbed, setIsDubbed] = useState(false);
  const [isCensored, setIsCensored] = useState(false);
  const [isSubbed, setIsSubbed] = useState(false);
  const [isOnlinePlay, setIsOnlinePlay] = useState(false);

  const handleScoreSlider = (e: any) => {
    const newValue = parseInt(e.target.value);
    if (e.target.id === "score-range-input") {
      setScorerSlider(newValue);
    }
  };

  // const handleToggleIsMovie = () => {
  //   setIsSeries(false);
  //   setIsMovie(true);
  // };

  // const handleToggleSeries = () => {
  //   setIsMovie(false);
  //   setIsSeries(true);
  // };

  const handleToggleDub = () => {
    setIsDubbed(!isDubbed);
  };

  const handleToggleCensor = () => {
    setIsCensored(!isCensored);
  };

  const handleToggleSub = () => {
    setIsSubbed(!isSubbed);
  };

  const handleToggleOnlinePlay = () => {
    setIsOnlinePlay(!isOnlinePlay);
  };

  const onClickAdvanceSearch = (e: any) => {
    e.preventDefault(); // Prevent the form from reloading the page
    onSearch();
  };

  return (
    <>
      <div className="flex justify-center items-center w-full h-full bg-red-400 font-vazir mx-auto p-10">
        <div className="flex w-[80%] h-[18rem] justify-around items-center bg-[#242424] rounded-md gap-2">
          <form
            id="form"
            name="form"
            dir="rtl"
            className="w-full h-full text-white bg-[#242424] grid grid-cols-4 grid-rows-1 justify-center items-center gap-x-10 ml-2"
          >
            <div className="flex flex-col w-full h-full justify-center items-center gap-0">
              <div className="flex justify-center items-center w-[60%] h-full gap-3">
                <p className="text-md">نوع</p>
                <div className="flex bg-[#1c1c1c] gap-0 rounded-full w-full h-[45%] items-center justify-center px-2 py-1 cursor-pointer">
                  <div
                    onClick={onHandleToggleIsMovie}
                    className={`flex text-xs justify-center items-center text-center w-full h-full rounded-full ${
                      isMovie ? "bg-orange-500" : ""
                    }`}
                  >
                    <p className="text-center">فیلم</p>
                  </div>
                  <div
                    onClick={onHandleToggleSeries}
                    className={`flex text-xs justify-center items-center text-center w-full h-full rounded-full ${
                      isSeries ? "bg-orange-500 " : ""
                    }`}
                  >
                    <p className="text-center">سریال</p>
                  </div>
                </div>
              </div>

              <div className="flex w-full h-full justify-center items-center gap-3">
                <label htmlFor="country">کشور</label>
                <select
                  autoComplete="true"
                  id="country"
                  name="country"
                  form="form"
                  className="text-xs text-gray-400 w-full h-[50%] bg-[#1c1c1c] border-none outline-none rounded-full"
                >
                  <option
                    defaultValue={"همه"}
                    className="bg-[#1c1c1c] text-gray-400"
                  >
                    همه
                  </option>
                  <option
                    defaultValue={"همه"}
                    value={"امریکا"}
                    className="bg-[#1c1c1c] text-gray-400"
                  >
                    آمریکا
                  </option>

                  <option
                    defaultValue={"همه"}
                    value={"اروپا"}
                    className="bg-[#1c1c1c] text-gray-400"
                  >
                    اروپا
                  </option>
                </select>
              </div>

              <div className="flex w-full h-full  justify-center items-center gap-3">
                <label htmlFor="quality">کیفیت</label>
                <select
                  value={quality}
                  onChange={onQualityChange}
                  autoComplete="true"
                  id="quality"
                  name="quality"
                  form="form"
                  className="text-xs text-gray-400 w-full h-[50%] bg-[#1c1c1c] border-none outline-none rounded-full"
                >
                  <option value={"همه"}>همه</option>
                  <option value={"HD"}>HD</option>
                  <option value={"Full HD"}>Full HD</option>
                </select>
              </div>
            </div>

            <div className="flex flex-col w-full h-full justify-center items-center gap-2">
              <div className="flex w-full h-full justify-center items-center gap-3">
                <label htmlFor="director">کارگردان</label>
                <input
                  id="director"
                  name="director"
                  value={director}
                  onChange={onDirectorChange}
                  placeholder="نام کارگردان"
                  className="text-xs pr-1 w-full h-[50%] text-gray-400 bg-[#1c1c1c] border-none outline-none rounded-full"
                ></input>
              </div>
              <div className="flex w-full h-full justify-center items-center gap-3">
                <label htmlFor="age">سن</label>
                <select
                  autoComplete="true"
                  id="age"
                  name="age"
                  form="form"
                  className="text-xs text-gray-400 w-full h-[50%] bg-[#1c1c1c] border-none outline-none rounded-full px-1"
                >
                  <option defaultValue={"همه"}>همه</option>
                </select>
              </div>
              <div className="flex w-full h-full justify-center items-center gap-3">
                <label htmlFor="network">شبکه</label>
                <select
                  autoComplete="true"
                  id="network"
                  name="network"
                  form="form"
                  className="text-xs text-gray-400 w-full h-[50%] bg-[#1c1c1c] border-none outline-none rounded-full px-1"
                >
                  <option defaultValue={"همه"}>همه</option>
                </select>
              </div>
            </div>
            <div className="flex flex-col w-full h-full justify-center items-center gap-2">
              <div className="flex w-full h-full  justify-center items-center gap-3">
                <label htmlFor="actors">بازیگران</label>
                <input
                  id="actors"
                  name="actors"
                  value={actor}
                  onChange={onActorChange}
                  placeholder="نام بازیگر"
                  className="text-xs pr-1 w-full h-[50%] text-gray-400 bg-[#1c1c1c] border-none outline-none rounded-full"
                ></input>
              </div>
              <div className="flex w-full h-full  justify-center items-center gap-3">
                <label htmlFor="genre">ژانر</label>
                <select
                  onChange={onGenreChange}
                  value={genre}
                  autoComplete="true"
                  id="genre"
                  name="genre"
                  form="form"
                  className="text-xs text-gray-400 w-full h-[50%] bg-[#1c1c1c] border-none outline-none rounded-full px-1"
                >
                  <option value={"همه"}>همه</option>
                  <option value={"اکشن"}>اکشن</option>
                  <option value={"علمی تخیلی"}>علمی تخیلی</option>
                  <option value={"ترسناک"}>ترسناک</option>
                  <option value={"رازآلود"}>رازآلود</option>
                  <option value={"هیجان انگیز"}>هیجان انگیز</option>
                  <option value={"درام"}>درام</option>
                  <option value={"ماجراجویی"}>ماجراجویی</option>
                  <option value={"جنایی"}>جنایی</option>
                </select>
              </div>
              <div className="flex w-full h-full  justify-center items-center gap-3">
                <label htmlFor="sort">ترتیب</label>
                <select
                  autoComplete="true"
                  id="sort"
                  name="sort"
                  form="form"
                  onChange={onOrderChange}
                  value={order}
                  className="text-xs text-gray-400 w-full h-[50%] bg-[#1c1c1c] border-none outline-none rounded-full px-1"
                >
                  <option defaultValue={"همه"}>همه</option>
                  <option value={"بالاترین امتیاز imdb"}>
                    بالاترین امتیاز imdb
                  </option>
                  <option value={"بالاترین رای imdb"}>بالاترین رای imdb</option>
                  <option value={"بیشترین امتیاز منتقدین"}>
                    بیشترین امتیاز منتقدین
                  </option>
                </select>
              </div>
            </div>
            <div className="flex flex-col w-full h-full items-center">
              <div className="relative w-full h-full">
                <label htmlFor="year-range-input">سال ساخت</label>
                <input
                  dir="ltr"
                  id="year-slider"
                  type="range"
                  min={1889}
                  max={2024}
                  value={maxYearSlider}
                  onChange={onMaxYearSliderChange}
                  className="w-full h-2
                    rounded-lg
                    cursor-pointer 
                    dark:bg-gray-700
                    appearance-none 
                    [&::-webkit-slider-runnable-track]:rounded-full [&::-webkit-slider-runnable-track]:bg-[#050505] [&::-webkit-slider-thumb]:appearance-none [&::-webkit-slider-thumb]:rotate-45 [&::-webkit-slider-thumb]:h-[.75rem] [&::-webkit-slider-thumb]:w-[.75rem] [&::-webkit-slider-thumb]:rounded-sm  [&::-webkit-slider-thumb]:bg-[#EB8307]"
                ></input>

                <span className="text-sm text-[#EB8307] dark:text-gray-400 absolute end-0 -top-0">
                  {maxYearSlider}
                </span>
              </div>

              <div className="relative w-full h-full">
                <label htmlFor="year-range-input"></label>
                <input
                  dir="ltr"
                  id="year-slider"
                  type="range"
                  min={1889}
                  max={2024}
                  value={minYearSlider}
                  onChange={onMinYearSliderChange}
                  className="w-full h-2
                    rounded-lg
                    cursor-pointer 
                    dark:bg-gray-700
                    appearance-none 
                    [&::-webkit-slider-runnable-track]:rounded-full [&::-webkit-slider-runnable-track]:bg-[#050505] [&::-webkit-slider-thumb]:appearance-none [&::-webkit-slider-thumb]:rotate-45 [&::-webkit-slider-thumb]:h-[.75rem] [&::-webkit-slider-thumb]:w-[.75rem] [&::-webkit-slider-thumb]:rounded-sm  [&::-webkit-slider-thumb]:bg-[#EB8307]"
                ></input>

                <span className="text-sm text-[#EB8307] dark:text-gray-400 absolute end-0 -top-4">
                  {minYearSlider}
                </span>
              </div>

              <div className="relative mb-6 w-full h-full ">
                <label htmlFor="score-range-input">امتیاز</label>
                <input
                  dir="ltr"
                  id="score-range-input"
                  type="range"
                  min={0}
                  max={10}
                  onChange={handleScoreSlider}
                  value={scoreSlider}
                  className="w-full h-2 
                  rounded-lg 
                  appearance-none 
                  cursor-pointer 
                  bg-tranparent
                  dark:bg-gray-700
                  [&::-webkit-slider-runnable-track]:rounded-full [&::-webkit-slider-runnable-track]:bg-[#050505] [&::-webkit-slider-thumb]:appearance-none [&::-webkit-slider-thumb]:rotate-45 [&::-webkit-slider-thumb]:h-[.75rem] [&::-webkit-slider-thumb]:w-[.75rem] [&::-webkit-slider-thumb]:rounded-sm  [&::-webkit-slider-thumb]:bg-[#EB8307]"
                ></input>

                <span className="text-sm text-[#EB8307] dark:text-gray-400 absolute end-0 -top-0">
                  {scoreSlider}
                </span>
              </div>
            </div>
            <div className="bg-[#242424] w-full h-full col-span-4 flex justify-between p-2">
              <div
                className="flex justify-center items-center gap-1 cursor-pointer"
                onClick={handleToggleDub}
              >
                <div className="text-sm">دوبله فارسی</div>
                {!isDubbed ? (
                  <FaToggleOff size={28}></FaToggleOff>
                ) : (
                  <FaToggleOn size={28} color="#EB8307"></FaToggleOn>
                )}
              </div>
              <div
                className="flex justify-center items-center gap-1 cursor-pointer"
                onClick={handleToggleCensor}
              >
                <div className="text-sm">سانسور شده</div>
                {!isCensored ? (
                  <FaToggleOff size={28}></FaToggleOff>
                ) : (
                  <FaToggleOn size={28} color="#EB8307"></FaToggleOn>
                )}
              </div>
              <div
                className="flex justify-center items-center gap-1 cursor-pointer"
                onClick={handleToggleSub}
              >
                <div className="text-sm">زیرنویس</div>
                {!isSubbed ? (
                  <FaToggleOff size={28}></FaToggleOff>
                ) : (
                  <FaToggleOn size={28} color="#EB8307"></FaToggleOn>
                )}
              </div>

              <div
                className="flex justify-center items-center gap-1 cursor-pointer"
                onClick={handleToggleOnlinePlay}
              >
                <div className="text-sm">پخش آنلاین</div>
                {!isOnlinePlay ? (
                  <FaToggleOff size={28}></FaToggleOff>
                ) : (
                  <FaToggleOn size={28} color="#EB8307"></FaToggleOn>
                )}
              </div>

              {/* search submit */}

              <div className="h-full w-[50%]" dir="ltr">
                <div className="flex justify-start items-center w-full h-full">
                  <div className="bg-[#050505] w-[45%] h-full flex justify-center items-center py-1.5 px-2 rounded-2xl">
                    <button
                      onClick={onClickAdvanceSearch}
                      className="bg-[#050505] text-gray-400 outline-none w-full text-left"
                      type="submit"
                    >
                      Search here...
                    </button>
                    <div className="flex justify-between items-center relative">
                      <CiSearch
                        size={20}
                        color="#EB8307"
                        className="absolute right-[100%]"
                      ></CiSearch>
                      <button type="submit"></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </form>
          <div className="w-fit h-full text-white bg-[#1c1c1c]  flex flex-col justify-center items-center">
            <TbDatabaseSearch size={128} color="#EB8307"></TbDatabaseSearch>
            <p className="text-center text-[#EB8307]">
              جست و جو<br></br> حرفه ای
            </p>
          </div>
        </div>
      </div>
    </>
  );
};

export default Search;
