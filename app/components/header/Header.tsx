"use client";
import lightLogo from "@/public/logo-light.png";
import { useState } from "react";
import Image from "next/image";
import { IoMdMoon } from "react-icons/io";
import { IoSunny } from "react-icons/io5";
import { FaCircle } from "react-icons/fa";
import { GiFilmStrip } from "react-icons/gi";
import { FaDisplay } from "react-icons/fa6";
import { FaMasksTheater } from "react-icons/fa6";
import { BiSupport } from "react-icons/bi";
import { AiOutlineHome } from "react-icons/ai";
import { CiSearch } from "react-icons/ci";
import { AiOutlineUserAdd } from "react-icons/ai";
import { TbLogin2 } from "react-icons/tb";

import ListItem from "../item/ListItem";

const Header = () => {
  const [nightMode, setNightMode] = useState(true);

  const nightModeHandler = () => {
    setNightMode((prev) => !prev);
  };

  return (
    <>
      <header className="grid grid-cols-2 gap-y-2 mb-2 pt-4 w-full h-full">
        <div className="bg-[#050505] h-full w-[100%]">
          <div className="w-full h-full flex items-center justify-around bg-[#050505]">
            <div className="flex gap-8 items-center w-fit">
              <Image src={lightLogo} width={224} alt="dark-mode-logo"></Image>
              <div className="w-20 h-7 bg-[#1E1E1E] rounded-xl flex items-center justify-between relative">
                <IoSunny size={28} color="#FCB501"></IoSunny>
                <FaCircle
                  className={`absolute transition-all delay-150 ease-in-out ${
                    !nightMode ? " left-[60%]" : "left-0"
                  }`}
                  onClick={nightModeHandler}
                  size={30}
                  color="white"
                ></FaCircle>
                <IoMdMoon size={28} color="#FCB501"></IoMdMoon>{" "}
              </div>
            </div>
          </div>
        </div>
        {/* right side auth buttons */}
        <div className="bg-[#050505] h-full w-[80%]">
          <div className="flex justify-end items-center w-full h-full gap-1">
            <div className="flex">
              <button className="inline-block bg-[#1E1E1E] hover:bg-[#EB8307] rounded-xl w-[8rem] h-[2rem] text-white relative">
                Sign In
                <TbLogin2
                  size={24}
                  color="white"
                  className="absolute top-[15%] left-[75%]"
                ></TbLogin2>
              </button>
            </div>
            <div>
              <button className="inline-block bg-[#1E1E1E] hover:bg-[#EB8307] rounded-xl w-[8rem] h-[2rem] text-white relative">
                Register
                <AiOutlineUserAdd
                  size={24}
                  color="white"
                  className="absolute top-[15%] left-[75%]"
                ></AiOutlineUserAdd>
              </button>
            </div>
          </div>
        </div>

        <div className="bg-[#1e1e1e] h-full w-[100%]">
          <div className="flex justify-center items-center w-full h-full">
            <div className="bg-[#050505] w-[45%] h-fit flex justify-center items-center py-1.5 px-2 rounded-2xl">
              <input
                className="bg-[#050505] text-gray-400 outline-none w-full"
                placeholder="Search here..."
                type="search"
              ></input>
              <div className="flex justify-between items-center relative">
                <CiSearch
                  size={20}
                  color="#EB8307"
                  className="absolute right-[100%]"
                ></CiSearch>
                <button type="submit"></button>
              </div>
            </div>
          </div>
        </div>
        <div className="bg-[#1e1e1e] h-full w-[100%]">
          <div className="flex justify-between items-center w-[80%] h-full">
            <nav className="flex items-center gap-1.5">
              <div className="flex flex-col gap-1">
                <span className="text-white text-right font-vazir">
                  ارتباط با ما
                </span>
                <span className="text-[#EB8307] text-[.75rem]">CONTACT</span>
              </div>
              <BiSupport
                size={24}
                color="#EB8307"
                className="hover:scale-125 transform transition-all"
              ></BiSupport>
            </nav>

            <nav className="flex items-center gap-1.5 group relative dropdown justify-center">
              <div className="flex flex-col gap-1">
                <span className="text-white text-right font-vazir">
                  هنرمندان
                </span>
                <span className="text-[#EB8307] text-[.75rem]">ARTISTS</span>
              </div>
              <FaMasksTheater
                size={24}
                color="#EB8307"
                className="hover:scale-125 transform transition-all"
              ></FaMasksTheater>
              <div className="group-hover:block hidden bg-transparent dropdown-menu absolute h-auto  top-[100%] w-[24rem] z-10">
                <div className="bg-[#1E1E1E] w-full h-full mt-2">
                  <div className="bg-[#1E1E1E] h-4 w-4 absolute left-[50%] -top-[1%] rotate-45"></div>
                  <ul className="grid grid-cols-2">
                    <ListItem title="movie"></ListItem>
                    <ListItem title="movie"></ListItem>
                    <ListItem title="movie"></ListItem>
                    <ListItem title="movie"></ListItem>
                  </ul>
                </div>
              </div>
            </nav>

            <nav className="flex items-center gap-1.5 group relative dropdown justify-center">
              <div className="flex flex-col gap-1">
                <span className="text-white text-right font-vazir">
                  دانلود سریال
                </span>
                <span className="text-[#EB8307] text-[.75rem]">SERIES</span>
              </div>
              <FaDisplay
                size={24}
                color="#EB8307"
                className="hover:scale-125 transform transition-all"
              ></FaDisplay>
              <div className="group-hover:block hidden bg-transparent dropdown-menu absolute h-auto  top-[100%] w-[24rem] z-10">
                <div className="bg-[#1E1E1E] w-full h-full mt-2">
                  <div className="bg-[#1E1E1E] h-4 w-4 absolute left-[50%] -top-[1%] rotate-45"></div>
                  <ul className="grid grid-cols-2">
                    <ListItem title="movie"></ListItem>
                    <ListItem title="movie"></ListItem>
                    <ListItem title="movie"></ListItem>
                    <ListItem title="movie"></ListItem>
                  </ul>
                </div>
              </div>
            </nav>

            <nav className="flex items-center gap-1.5 group relative dropdown justify-center">
              <div className="flex flex-col gap-1">
                <span className="text-white text-right font-vazir">
                  دانلود فیلم
                </span>
                <span className="text-[#EB8307] text-[.75rem]">MOVIES</span>
              </div>
              <GiFilmStrip
                size={24}
                color="#EB8307"
                className="hover:scale-125 transform transition-all"
              ></GiFilmStrip>
              <div className="group-hover:block hidden bg-transparent dropdown-menu absolute h-auto  top-[100%] w-[24rem] z-10">
                <div className="bg-[#1E1E1E] w-full h-full mt-2">
                  <div className="bg-[#1E1E1E] h-4 w-4 absolute left-[50%] -top-[1%] rotate-45"></div>
                  <ul className="grid grid-cols-2">
                    <ListItem title="movie"></ListItem>
                    <ListItem title="movie"></ListItem>
                    <ListItem title="movie"></ListItem>
                    <ListItem title="movie"></ListItem>
                  </ul>
                </div>
              </div>
            </nav>

            <nav className="flex items-center gap-1.5">
              <div className="flex flex-col gap-1">
                <span className="text-white text-right font-vazir">خانه</span>
                <span className="text-[#EB8307] text-[.75rem]">HOME</span>
              </div>
              <AiOutlineHome
                size={24}
                color="#EB8307"
                className="hover:scale-125 transform transition-all"
              ></AiOutlineHome>
            </nav>
          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
