"use client";
import Header from "../header/Header";
import Carousel from "../carousel/Carousel";
import Search from "../search/Search";
import Media from "../media/Media";
import { useState } from "react";

export default function ClientHome({ media }: any) {
  const [isMovie, setIsMovie] = useState(true); // Default to movies
  const [isSeries, setIsSeries] = useState(false);
  const [actor, setActor] = useState("");
  const [director, setDirector] = useState("");
  const [genre, setGenre] = useState("");
  const [quality, setQuality] = useState("");
  const [maxYearSlider, setMaxYearSlider] = useState(2024);
  const [minYearSlider, setMinYearSlider] = useState(1890);
  const [order, setOrder] = useState("همه");

  // fixed number issue with movies length in movies category
  //  add series to rightside
  // add series episode and season
  // add dubbed serial at right side
  // movie filter
  // series filter
  // trailers

  const handleToggleIsMovie = () => {
    setIsMovie(true);
    setIsSeries(false);
    alert(`is movie ${isMovie} is series ${isSeries}`);
  };

  const handleToggleSeries = () => {
    setIsMovie(false);
    setIsSeries(true);
    alert(`is movie ${isMovie} is series ${isSeries}`);
  };

  const changeActor = (e: any) => {
    e.preventDefault();
    setActor(e.target.value);
  };

  const changeDirector = (e: any) => {
    e.preventDefault();
    setDirector(e.target.value);
  };

  const changeGenre = (e: any) => {
    e.preventDefault();
    setGenre(e.target.value);
  };

  const changeGenreFromMedia = (genre: string) => {
    setGenre(genre);
    handleSearch();
  };

  const changeQuality = (e: any) => {
    e.preventDefault();
    setQuality(e.target.value);
  };

  const changeOrder = (e: any) => {
    e.preventDefault();
    setOrder(e.target.value);
  };

  const handleMaxYearSlider = (e: any) => {
    const newValue = parseInt(e.target.value);
    setMaxYearSlider(newValue);
  };

  const handleMinYearSlider = (e: any) => {
    const newValue = parseInt(e.target.value);
    setMinYearSlider(newValue);
  };

  let series = media.filter((item: any) => item.type === "series");

  // only movies list
  const [filteredMovies, setFilteredMovies] = useState(
    media.filter((item: any) => item.type === "movie")
  );

  function covertVotes(votes: any) {
    let temp = "";
    if (votes.endsWith("M")) {
      temp = votes.replace("M", "");
      votes = parseFloat(temp) * 1000000;
      return votes;
    } else if (votes.endsWith("K")) {
      temp = votes.replace("K", "");
      votes = parseFloat(temp) * 1000;
      return votes;
    } else {
      return parseFloat(votes.replace(/,/g, "")); // Default case, just remove commas and convert to integer
    }
  }

  // Perform the Advance search logic
  const handleSearch = () => {
    let filtered = media.filter((item: any) => {
      const typeMatch =
        (isMovie && item.type === "movie") ||
        (isSeries && item.type === "series");
      const actorMatch =
        !actor ||
        item.stars.some((star: any) =>
          star.trim().toLowerCase().includes(actor.toLowerCase())
        );
      const directorMatch =
        !director ||
        item.director.toLowerCase().includes(director.toLowerCase());
      const genreMatch =
        !genre ||
        genre === "همه" ||
        item.genre.some((g: any) => g.toLowerCase() === genre.toLowerCase());
      const qualityMatch =
        quality === "همه" || !quality || item.quality === quality;
      const yearMatch =
        item.year >= minYearSlider && item.year <= maxYearSlider;

      return (
        typeMatch &&
        actorMatch &&
        directorMatch &&
        genreMatch &&
        qualityMatch &&
        yearMatch
      );
    });

    if (order === "بالاترین امتیاز imdb") {
      filtered.sort((a: any, b: any) => b.imdbscore - a.imdbscore);
    } else if (order === "بالاترین رای imdb") {
      filtered.sort(
        (a: any, b: any) => covertVotes(b.imdbVotes) - covertVotes(a.imdbVotes)
      );
    } else if (order === "بیشترین امتیاز منتقدین") {
      filtered.sort((a: any, b: any) => b.metaCritics - a.metaCritics);
    }

    setFilteredMovies(filtered);
  };

  let slides: any = [];

  for (let i = media.length - 1; i > 1; i--) {
    if (media[i].type === "movie") {
      slides.push({
        key: media[i].id,
        firstImage: media[i].firstImage,
        secondImage: media[i].secondImage,
        title: media[i].title,
        score: parseFloat(media[i].imdbscore) * 10,
      });
    }
  }

  type GenreCounts = { [key: string]: number };
  const genreCounts = media.reduce((acc: any, item: any) => {
    if (item.type === "movie") {
      item.genre.forEach((genre: any) => {
        acc[genre] = (acc[genre] || 0) + 1;
      });
    }
    return acc;
  }, {} as GenreCounts);

  return (
    <div>
      <div className="bg-[#050505]">
        <Header></Header>
        <div className="w-[100%] h-full m-auto p-10 bg-[#4e37d3]">
          <Carousel slides={slides}></Carousel>
        </div>
        <Search
          actor={actor}
          director={director}
          genre={genre}
          quality={quality}
          order={order}
          maxYearSlider={maxYearSlider}
          minYearSlider={minYearSlider}
          onActorChange={changeActor}
          onDirectorChange={changeDirector}
          onGenreChange={changeGenre}
          onQualityChange={changeQuality}
          onOrderChange={changeOrder}
          onMaxYearSliderChange={handleMaxYearSlider}
          onMinYearSliderChange={handleMinYearSlider}
          onSearch={handleSearch}
          isMovie={isMovie}
          onHandleToggleIsMovie={handleToggleIsMovie}
          isSeries={isSeries}
          onHandleToggleSeries={handleToggleSeries}
        ></Search>
        <Media
          movies={filteredMovies}
          series={series}
          media={media}
          changeGenreFromMedia={changeGenreFromMedia}
          genreCounts={genreCounts}
        />
      </div>
    </div>
  );
}
