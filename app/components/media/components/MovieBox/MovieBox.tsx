import { MdOutlineFavoriteBorder } from "react-icons/md";
import { IoMdAdd } from "react-icons/io";
import { PiTelevisionSimpleBold } from "react-icons/pi";
import { CiClock2 } from "react-icons/ci";
import { FaFolderOpen } from "react-icons/fa6";
import { GrUserManager } from "react-icons/gr";
import { FaMasksTheater } from "react-icons/fa6";
import { TfiWorld } from "react-icons/tfi";
import { SiSubtitleedit } from "react-icons/si";
import { BiSolidMoviePlay } from "react-icons/bi";
import { SiMetacritic } from "react-icons/si";

import Image from "next/image";

import SubTitleText from "@/app/components/media/components/MovieBox/SubTitleText";
import DownloadButton from "./DownloadButton";
const MovieBox = ({ movie }: any) => {
  function formatArray(arr: any) {
    if (arr.length > 1) {
      return arr.join(", ");
    } else {
      return arr.join(" ");
    }
  }

  return (
    <>
      <div className="bg-[#242424] flex justify-between w-full h-[420.85px] p-1 rounded-md mb-12">
        <div className="bg-[#242424] h-full w-full flex justify-center items-center relative text-white">
          <div
            className="flex flex-col justify-evenly font-vazir bg-[#242424] w-full h-full px-4 mb-2 text-[.9rem]"
            dir="rtl"
          >
            <h1>
              دانلود فیلم {movie.title.toUpperCase()} {movie.year}
            </h1>
            <div className="flex items-center">
              <PiTelevisionSimpleBold
                className="ml-1"
                color="#EB8307"
              ></PiTelevisionSimpleBold>
              کیفیت:
              <span>{movie.quality}</span>
            </div>
            <div className="flex items-center">
              <CiClock2 className="ml-1" color="#EB8307"></CiClock2>
              زمان:
              <span>{movie.time} دقیقه</span>
            </div>
            <div className="flex items-center">
              <FaFolderOpen className="ml-1" color="#EB8307"></FaFolderOpen>
              ژانر:
              <span>{formatArray(movie.genre)}</span>
            </div>
            {movie.director ? (
              <div className="flex items-center">
                <GrUserManager className="ml-1" color="#EB8307"></GrUserManager>
                کارگردان:
                <span>{movie.director}</span>
              </div>
            ) : (
              ""
            )}
            <div className="flex items-center">
              <FaMasksTheater className="ml-1" color="#EB8307"></FaMasksTheater>
              ستارگان:
              <span>{formatArray(movie.stars)}</span>
            </div>
            <div className="flex items-center">
              <TfiWorld className="ml-1" color="#EB8307"></TfiWorld>
              محصول کشور:
              <span>{formatArray(movie.placeOfProduction)}</span>
            </div>

            {movie.type === "movie" && (
              <div className="flex items-center gap-2">
                <div
                  className={` ${
                    movie.metaCritics >= 60
                      ? "bg-[#66CC33]"
                      : movie.metaCritics >= 40
                      ? "bg-[#EB8307]"
                      : "bg-red-600"
                  } w-[30px] h-[30px] flex items-center justify-center rounded-[5px] text-sm`}
                >
                  {movie.metaCritics}
                </div>
                <div>
                  <SiMetacritic size={20} color="#EB8307"></SiMetacritic>
                </div>
                <div>امتیاز منتقدین</div>
              </div>
            )}

            <div className="text-sm">{movie.summary}</div>
          </div>
          <div className="flex flex-col justify-center items-center absolute top-[7%] left-[3%]">
            <div className="border-b border-gray-500 w-full flex text-white justify-center items-center">
              <p className="text-[#EB8307] font-bold">{movie.imdbscore}</p>/
              <p className="text-white text-xs">10</p>
            </div>
            <div className="text-white">{movie.imdbVotes}</div>
            <div className="text-white font-extrabold bg-[#EB8307] py-.5 px-1 rounded-md">
              IMDb
            </div>
          </div>
          <div className="cursor-pointer p-1 absolute left-[15%] top-[8%] bg-[#242424] rounded-full border border-gray-500">
            <MdOutlineFavoriteBorder
              color="white"
              size={24}
            ></MdOutlineFavoriteBorder>
            <div className="text-sm bg-gray-600 rounded-full absolute -right-[30%] top-[30%]">
              <IoMdAdd size={13} color="#EB8307"></IoMdAdd>
            </div>
          </div>
          <div className="peer absolute -rotate-45 right-[97%] flex justify-center items-center bg-[#8BC34A] w-12 h-12  rounded-md hover:shadow-[0px_0px_0px_2.5px_rgba(191,229,144,0.3)] delay-50 hover:ease-in-out hover:duration-300">
            <SiSubtitleedit className="rotate-45" size={20}></SiSubtitleedit>
          </div>
          <SubTitleText></SubTitleText>
          <DownloadButton></DownloadButton>
        </div>
        <div className="bg-[#242424] h-full w-[250px] flex flex-col p-1 justify-between">
          <div className="flex justify-center items-center bg-[#242424] w-full h-full p-1">
            <Image
              width={181}
              height={267}
              className="w-full rounded-lg"
              src={movie.secondImage}
              alt="temp"
            />
          </div>
          <div className="group flex justify-center items-center h-[50px] w-full bg-[#242424] mb-10">
            <div className="bg-[#2D2D2D] w-1/2 h-full flex justify-center items-center rounded-l-md">
              <button className="group-hover:text-[#EB8307] text-white font-vazir text-sm">
                مشاهده تریلر
              </button>
            </div>
            <div className="rounded-r-md cursor-pointer group-hover:bg-[#EB8307] bg-[#1C1C1C] w-1/2 h-full flex justify-center items-center border-[#2D2D2D] border-l-[30px] border-b-[50px] border-b-transparent relative">
              <div className="absolute top-3 left-2">
                <BiSolidMoviePlay
                  size={28}
                  className="text-[#EB8307] group-hover:text-white"
                ></BiSolidMoviePlay>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MovieBox;
