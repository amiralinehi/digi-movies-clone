import { FiDownload } from "react-icons/fi";

const DownloadButton = () => {
  return (
    <>
      <div className="peer border-4 border-[#050505] rounded-lg  w-[40px] h-[40px] rotate-45  absolute -bottom-6 left-[20%] flex justify-center items-center">
        <div className="peer absolute bg-orange-500 w-8 h-8  rounded-md flex justify-center items-center">
          <FiDownload className="-rotate-45"></FiDownload>
        </div>
        <div className="peer-hover:flex hidden absolute  w-10 h-10 -top-7 -left-7 rotate-45   rounded-md  justify-center items-center">
          <p className="text-nowrap -rotate-90 font-vazir text-[.75rem]">
            ادامه<span className="text-orange-500 mx-1">/</span>دانلود
          </p>
        </div>
      </div>
    </>
  );
};

export default DownloadButton;
