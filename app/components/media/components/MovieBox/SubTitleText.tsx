const SubTitleText = () => {
  return (
    <>
      <div className="peer text-[.75rem]  justify-center items-center  text-center peer-hover:flex hidden font-vazir  rounded-r-lg absolute bg-black w-[175px] h-[25px] text-nowrap left-12">
        همراه با زیرنویس چسبیده
        <div className="top-1 -left-2 absolute bg-black w-[18px]  h-[18px] rotate-45 "></div>
      </div>
    </>
  );
};

export default SubTitleText;
