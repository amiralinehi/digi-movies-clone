import React from "react";
import Image from "next/image";

const SeriesBox = ({ serial }: any) => {
  return (
    <div className="flex flex-col justify-center items-center w-full h-full font-vazir text-sm mb-1 p-1">
      <a title={serial.title} className=" w-full">
        <div className="cover w-full">
          <Image
            src={serial.secondImage}
            alt={serial.title}
            width={150}
            height={269}
            className="w-full h-[269px] rounded-t-md object-cover"
            quality={100}
          />
        </div>
        <div className="bg-[#1B1B1B]  text-white w-full h-full text-center rounded-b-md text-xs whitespace-nowrap py-2">
          قسمت{" "}
          <span className="text-xs w-4 h-4 bg-orange-400 rounded-full inline-flex justify-center items-center">
            5
          </span>{" "}
          فصل <span className="text-xs">4</span> با زیرنویس منتشر شد
        </div>
      </a>
    </div>
  );
};

export default SeriesBox;
