import MovieBox from "./components/MovieBox/MovieBox";
import SeriesBox from "./components/SeriesBox/SeriesBox";
import { ImTv } from "react-icons/im";
import { RiMovie2Line } from "react-icons/ri";
import { IoMdMenu } from "react-icons/io";
import { useMemo } from "react";

const Media = ({ movies, series, changeGenreFromMedia, genreCounts }: any) => {
  const handleGenreClick = (genre: string) => {
    changeGenreFromMedia(genre); // Update genre in the parent component
  };

  return (
    <div className="bg-yellow-500 flex justify-center items-center p-10">
      <div className="bg-green-500 flex w-[80%] h-full gap-5">
        {/* Movie Boxes Section */}
        <section className="bg-orange-500 w-[80%] justify-center items-center">
          <div className="bg-[#050505] flex flex-col w-full h-full justify-start items-center py-1">
            {movies.map((movie: any) => (
              <MovieBox key={movie.id} movie={movie} />
            ))}
          </div>
        </section>

        {/* Series Boxes Section */}
        <section className="bg-sky-500 w-[20%] h-full">
          <section className="flex justify-center items-center font-vazir text-[.75] gap-1">
            <span className="flex flex-col font-semibold text-white">
              سریال های
              <span className="text-xs text-center text-white font-light">
                بروز شده
              </span>
            </span>
            <ImTv color="#EB8307" size={24}></ImTv>
          </section>
          <div className="flex flex-col h-full w-full bg-white justify-center items-center">
            {series.map((serial: any) => (
              <SeriesBox serial={serial} key={serial.id} />
            ))}
          </div>

          {/* Movie Category Section */}
          <section className="font-vazir">
            <div className="flex justify-center items-center">
              {/* Title Text */}
              <span className="flex flex-col font-semibold text-white">
                دسته بندی{" "}
                <span className="text-xs text-center text-white font-light">
                  فیلم ها{" "}
                </span>
              </span>

              {/* Title Icons */}
              <div className="flex justify-center items-center">
                <RiMovie2Line color="#EB8307" size={24}></RiMovie2Line>
                <IoMdMenu color="#fff" size={24}></IoMdMenu>
              </div>
            </div>
            <ul className="text-sm text-[#BEBEBE]">
              {Object.keys(genreCounts).map((genre) => (
                <li
                  key={genre}
                  className="flex justify-between items-center px-2 py-1 cursor-pointer"
                >
                  {/* Number of Movies */}
                  <div>{`(${genreCounts[genre]})`}</div>
                  {/* Genre Name */}
                  <div
                    onClick={() => handleGenreClick(genre)}
                  >{`${genre} <`}</div>
                </li>
              ))}
            </ul>
          </section>
        </section>
      </div>
    </div>
  );
};

export default Media;
