import RootLayout from "./layout";
import ClientHome from "./components/ClientHome/ClientHome";
export default function Home() {
  // fixed number issue with movies length in movies category
  //  add series to rightside
  // add series episode and season
  // add dubbed serial at right side
  // movie filter
  // series filter
  // trailers

  const media = [
    {
      type: "movie",
      title: "Inception",
      genre: ["اکشن", "علمی تخیلی"],
      summary:
        "دام کاب یک دزد ماهر در استخراج اسرار ارزشمند مورد نیاز سازمان‌های جاسوسی و شرکت‌های تجاری و چندملیتی است. توانایی او در این است که هنگامی که دیگران در خواب هستند و ذهنشان در آسیب پذیرترین وضعیت است...",
      quality: "HD",
      placeOfProduction: ["امریکا"],
      stars: [
        "leonardo DiCaprio",
        "joseph Gordon-Levitt",
        "ellen Page",
        "zendaya",
      ],
      director: "christopher nolan",
      time: "148",
      imdbscore: "8.8",
      imdbVotes: "2.5M",
      // imdbVotes: "2500000",
      firstImage: "/movies/inception/inception1.jpg",
      secondImage: "/movies/inception/inception2.jpg",
      metaCritics: "74",
      year: 2010,
      id: 1,
    },

    {
      type: "series",
      title: "House of the Dragon",
      genre: ["اکشن", "درام", "فانتزی", "ماجراجویی", "هیجان انگیز"],
      summary:
        "داستان خاندان تارگرین که دویست سال قبل از وقایع سریال «بازی تاج و تخت» و زاده شدن دنریس تارگرین جریان دارد.",
      quality: "Full HD",
      placeOfProduction: ["امریکا"],
      stars: ["emma d'arcy", "olivia cooke", "paddy considine", "matt smith"],
      director: "",
      time: "60",
      imdbscore: "8.4",
      imdbVotes: "387.1K",
      // imdbVotes: "2500000",
      firstImage: "/series/house of dragon/houseofdragon1.jpg",
      secondImage: "/series/house of dragon/houseofdragon2.jpg",
      metaCritics: "",
      year: 2022,
      status: "پایان فصل دوم",
      renewal: "تمدبد شده برای فصل سوم",
      id: 11,
      seasons: [
        {
          seasonNumber: 1,
          episodes: [
            {
              episodeNumber: 1,
              title: "The Heirs of the Dragon",
              summary: "توضیحی مختصر از اپیزود",
              duration: "60 min",
              downloadLink: "/series/house-of-dragon/s1/ep1/download", // Optional
              watchLink: "/series/house-of-dragon/s1/ep1/watch", // Optional
            },
            {
              episodeNumber: 2,
              title: "The Rogue Prince",
              summary: "توضیحی مختصر از اپیزود",
              duration: "62 min",
              downloadLink: "/series/house-of-dragon/s1/ep2/download",
              watchLink: "/series/house-of-dragon/s1/ep2/watch",
            },
            // Other episodes...
          ],
        },
        {
          seasonNumber: 2,
          episodes: [
            // Season 2 episodes
          ],
        },
      ],
    },

    {
      type: "series",
      title: "The Boys",
      genre: ["اکشن", "علمی تخیلی", "جنایی", "کمدی"],
      summary:
        "داستان در دنیایی اتفاق می‌افتد که ابرقهرمانان وجه تاریک از شهرت را در آغوش گرفته و به سمت فساد کشیده شده‌اند. در این میان، یک گروه از مبارزان خودسر که برای پلیس سیا کار می‌کنند، موظف می‌شوند تا به هر قیمتی که شده، ابرقهرمانان فاسد را نابود کنند تا...",
      quality: "HD",
      placeOfProduction: ["امریکا"],
      stars: ["karl urban", "jack quaid", "anthony starr"],
      director: "",
      time: "60",
      imdbscore: "8.7",
      imdbVotes: "664.7K",
      // imdbVotes: "2500000",
      firstImage: "/series/theboys/theboys1.jpg",
      secondImage: "/series/theboys/theboys2.jpg",
      metaCritics: "",
      year: 2019,
      id: 12,
      seasons: [
        {
          seasonNumber: 1,
          episodes: [
            {
              episodeNumber: 1,
              title: "The Name of the Game",
              duration: "60 min",
              downloadLink: "",
              watchLink: "",
            },
            {
              episodeNumber: 2,
              title: "Cherry",
              duration: "58 min",
              downloadLink: "",
              watchLink: "",
            },
          ],
        },
        {
          seasonNumber: 2,
          episodes: [
            {
              episodeNumber: 1,
              title: "The Big Ride",
              duration: "65 min",
              downloadLink: "",
              watchLink: "",
            },
            {
              episodeNumber: 2,
              title: "Proper Preparation and Planning",
              duration: "60 min",
              downloadLink: "",
              watchLink: "",
            },
          ],
        },
        {
          seasonNumber: 3,
          episodes: [
            {
              episodeNumber: 1,
              title: "Payback",
              duration: "66 min",
              downloadLink: "",
              watchLink: "",
            },
            {
              episodeNumber: 2,
              title: "The Only Man in the Sky",
              duration: "58 min",
              downloadLink: "",
              watchLink: "",
            },
          ],
        },
      ],
    },

    {
      type: "series",
      title: "Stranger Things",
      genre: ["علمی تخیلی", "درام", "ترسناک"],
      summary:
        "گروهی از بچه‌های جوان که با نیروهای ماوراءالطبیعه روبرو می‌شوند...",
      quality: "Full HD",
      placeOfProduction: ["امریکا"],
      stars: ["winona ryder", "david harbour", "millie bobby brown"],
      director: "duffer brothers",
      time: "50",
      imdbscore: "8.7",
      imdbVotes: "1.2M",
      firstImage: "/series/strangerthings/strangerthings1.jpg",
      secondImage: "/series/strangerthings/strangerthings2.jpg",
      metaCritics: "74",
      year: 2016,
      status: "پایان فصل چهارم",
      renewal: "تمدید شده برای فصل پنجم",
      id: 2,
      seasons: [
        {
          seasonNumber: 1,
          episodes: [
            {
              episodeNumber: 1,
              title: "The Vanishing of Will Byers",
              duration: "49 min",
              downloadLink: "",
              watchLink: "",
            },
            {
              episodeNumber: 2,
              title: "The Weirdo on Maple Street",
              duration: "47 min",
              downloadLink: "",
              watchLink: "",
            },
          ],
        },
      ],
    },

    {
      type: "series",
      title: "Breaking Bad",
      genre: ["جنایی", "درام", "تریلر"],
      summary:
        "یک معلم شیمی به جرم تولید متامفتامین به یک جنایتکار تبدیل می‌شود...",
      quality: "HD",
      placeOfProduction: ["امریکا"],
      stars: ["bryan cranston", "aaron paul", "anna gunn"],
      director: "vince gilligan",
      time: "47",
      imdbscore: "9.5",
      imdbVotes: "1.8M",
      firstImage: "/series/breakingbad/breakingbad1.jpg",
      secondImage: "/series/breakingbad/breakingbad2.jpg",
      metaCritics: "86",
      year: 2008,
      status: "پایان سریال",
      id: 4,
      seasons: [
        {
          seasonNumber: 1,
          episodes: [
            {
              episodeNumber: 1,
              title: "Pilot",
              duration: "58 min",
              downloadLink: "",
              watchLink: "",
            },
            {
              episodeNumber: 2,
              title: "Cat's in the Bag...",
              duration: "47 min",
              downloadLink: "",
              watchLink: "",
            },
          ],
        },
      ],
    },

    {
      type: "movie",
      title: "the conjuring",
      genre: ["ترسناک", "رازآلود", "هیجان انگیز"],
      summary:
        "دو متخصص مشهور امور ماوراءالطبیعه به نام اد و لورن وارن می خواهند عامل تسخیر یک خانه ی روستایی و وحشت زیاد ساکنان آنجا را کشف کنند. اما.",
      quality: "Full HD",
      placeOfProduction: ["امریکا"],
      stars: ["patrick wilson", "vera farmiga", "ron livingston"],
      director: "james wan",
      time: "112",
      imdbscore: "7.2",
      imdbVotes: "528K",
      // imdbVotes: "528000",
      firstImage: "/movies/the conjuring/theconjuring1.jpg",
      secondImage: "/movies/the conjuring/theconjuring2.jpg",
      metaCritics: "68",
      year: 2013,
      id: 2,
    },

    {
      type: "movie",
      title: "dune2",
      genre: ["اکشن", "درام", "علمی تخیلی", "ماجراجویی"],
      summary:
        "در دنباله قسمت اول، پاول آتریدیز با چانی و فرمن‌ها متحد می‌شود و به دنبال انتقام از توطئه‌گرانی است که خانواده‌اش را نابود کردن.",
      quality: "Full HD",
      placeOfProduction: ["امریکا", "گامبیا", "اردن", "امارات"],
      stars: ["timothée Chalamet", "zendaya", "rebecca ferguson"],
      director: "denis villeneuve",
      time: "166",
      imdbscore: "8.7",
      imdbVotes: "400K",
      // imdbVotes: "400000",
      firstImage: "/movies/dune/dune1.jpg",
      secondImage: "/movies/dune/dune2.jpg",
      metaCritics: "79",
      year: 2024,
      id: 3,
    },

    {
      type: "movie",
      title: "The Shawshank Redemption",
      genre: ["درام"],
      summary:
        "در سال ۱۹۴۶ اندی دوفرسن بانکدار نیو انگلندی که مردی معقول و متعادل است به اتهام قتل همسرو دوستش محکوم به حبس ابد درزندان شائوشنک می شود. پس از چندی در فعالیتهای غیر غانونی مالی با رئییس زندان همکاری کرده ودرعین حال برای خودش هم حسابی با یک شریک ...",
      quality: "Full HD",
      placeOfProduction: ["امریکا"],
      stars: ["tim Robbins", "morgan Freeman", "bob Gunton"],
      director: "Frank Darabont",
      time: "142",
      imdbscore: "9.2",
      imdbVotes: "2.9M",
      // imdbVotes: "2900000",
      firstImage: "/movies/the shawshank redemption/theshawshnak1.jpg",
      secondImage: "/movies/the shawshank redemption/theshawshnak2.jpg",
      metaCritics: "82",
      year: 1994,
      id: 4,
    },

    {
      type: "movie",
      title: "The dark knight",
      genre: ["درام", "اکشن", "جنایی", "هیجان انگیز"],
      summary:
        "بتمن به کمک سرگرد جیم گوردون و دادستان جدید هاروی دنت، شهر را از وجود آخرین خانواده‌های تبهکاری پاک می کند. همکاری این سه نفر ظاهرا موثر واقع میشود، اما طولی نمی کشد که خود را در مقابل هرج و مرج های یک تبهکار نو پا بنام جوکر، عاجز می بینند..",
      quality: "Full HD",
      placeOfProduction: ["امریکا"],
      stars: ["christian bale", "heath ledger", "aaron eckhart"],
      director: "christopher nolan",
      time: "152",
      imdbscore: "9",
      imdbVotes: "2.8M",
      // imdbVotes: "2800000",
      firstImage: "/movies/dark knight/darkknight1.jpg",
      secondImage: "/movies/dark knight/darkknight2.jpg",
      metaCritics: "84",
      year: 2012,
      id: 5,
    },

    {
      type: "movie",
      title: "the god father 1972",
      genre: ["درام", "جنایی"],
      summary:
        "دون ویتو کورلئونه» یکی از رؤسای پرقدرت مافیاست که به خاطر کمک هایش به مهاجران ایتالیایی در نیویورک به «پدرخوانده» معروف است. پس از مرگ «دون ویتو»، جای او را پسر کوچک خانواده، «مایکل»، می گیرد که پیش از این علاقه ای به فعالیت های غیرقانونی خانواده اش نداشته است. «مایکل» خیلی زود با یک سری قتل و کشتار و پس از تصفیه ی خرده حساب های قدیمی، پدرخوانده ی جدید می شود .",
      quality: "HD",
      placeOfProduction: ["امریکا"],
      stars: ["marlon brando", "al pacino", "james caan"],
      director: "francis ford coppola",
      time: "175",
      imdbscore: "9",
      imdbVotes: "2M",
      // imdbVotes: "2000000",
      firstImage: "/movies/thegodfather/thegodfather1.jpg",
      secondImage: "/movies/thegodfather/thegodfather2.jpg",
      metaCritics: "100",
      year: 1972,
      id: 6,
    },
    // Add more movie objects as needed
  ];

  // only series list

  return (
    <RootLayout>
      <div className="bg-[#050505]">
        <ClientHome media={media}></ClientHome>
      </div>
    </RootLayout>
  );
}
